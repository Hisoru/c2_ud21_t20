package C2_UD21.T20;

import dto.Geometria;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {
    
	Geometria cuadrado = new Geometria(1);
	Geometria circulo = new Geometria(2);
	Geometria triangulo = new Geometria(3);
	Geometria rectangulo = new Geometria(4);
	Geometria pentagono = new Geometria(5);
	Geometria rombo = new Geometria(6);
	Geometria romboide = new Geometria(7);
	Geometria trapecio = new Geometria(8);
	Geometria predeterminado = new Geometria();
	
	public void testGetId() {
		
		assertEquals(1, cuadrado.getId());
		
	}

	public void testSetId() {
		
		cuadrado.setId(1);
		assertEquals(1, cuadrado.getId());
		
	}
	
	public void testGetNom() {
		
		assertEquals("cuadrado", cuadrado.getNom());
		
	}
	
	public void testSetNom() {
		
		cuadrado.setNom("cuadrado");
		assertEquals("cuadrado", cuadrado.getNom());
		
	}
	
	public void testAreaCuadrado() {
		
		int resultado = Geometria.areacuadrado(4);
		int esperado = 16;
		assertEquals(resultado, esperado);
		
	}
	
	public void testAreaCirculo() {
		
		double resultado = Geometria.areaCirculo(2);
		double esperado = 12;
		double delta = 1;
		assertEquals(resultado, esperado, delta);
		
	}
	
	public void testAreaTriangulo() {
		
		int resultado = Geometria.areatriangulo(2, 5);
		int esperado = 5;
		assertEquals(resultado, esperado);
		
	}
	
	public void testAreaRectangulo() {
		
		int resultado = Geometria.arearectangulo(2, 5);
		int esperado = 10;
		assertEquals(resultado, esperado);
		
	}
	
	public void testAreaPentagono() {
		
		int resultado = Geometria.areapentagono(2, 5);
		int esperado = 6;
		int delta = 1;
		assertEquals(resultado, esperado, delta);
		
	}
	
	public void testAreaRombo() {
		
		int resultado = Geometria.arearombo(2, 5);
		int esperado = 5;
		assertEquals(resultado, esperado);
		
	}
	
	public void testAreaRomboide() {
		
		int resultado = Geometria.arearomboide(2, 3);
		int esperado = 6;
		assertEquals(resultado, esperado);
		
	}
	
	public void testAreaTrapecio() {
		
		double resultado = Geometria.areatrapecio(2, 2, 3);
		double esperado = 6;
		double delta = 1;
		assertEquals(resultado, esperado, delta);
		
	}
	
	public void testGetArea() {
		
		cuadrado.setArea(4);
		assertEquals(4, cuadrado.getArea(), 1);
		
	}
	
	public void testSetArea() {
		
		assertEquals(1, cuadrado.getId());
		
	}
	
	public void testToString() {
		
		assertEquals("Geometria [id=1, nom=cuadrado, area=0.0]", cuadrado.toString());
		
	}
	
}